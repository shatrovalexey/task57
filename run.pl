#!/usr/bin/perl

use strict ;
use warnings ;
use feature qw(state) ;

use Perl::Critic ;
use LWP::UserAgent ;
use URI::URL ;
use XML::LibXML ;
use Date::Parse qw(str2time) ;

# Предполагаю, что в сутках 24 часа, а в неделе 7 дней,
# а при парсинге HTML не нужно выводить ошибки.

use constant 'HOURS' => 1 .. 24 ;
use constant 'WDAYS' => 1 .. 7 ;
use constant 'XML_SETTINGS' =>
	'recover' => 1 ,
	'huge' => 1 ,
	'recover_silently' => 1 ,
	'suppress_warnings' => 1 ,
	'suppress_errors' => 1 ,
	'load_ext_dtd' => 0 ,
	'complete_attributes' => 0 ,
	'validation' => 0 ,
	'no_xinclude_nodes' => 1 ,
	'no_network' => 1 ,
	'no_cdata' => 1 ,
;
use constant 'UA_SETTINGS' => ( ) ;
use constant 'GIT_URL' => 'https://github.com/' ;

# Логин
my ( $login ) = @ARGV or die( 'No login passed' ) ;

# Что-то XML::LibXML не хочет загружать документ из аргумента 'location',
# приходится загружать строку с помощью LWP.
sub XML::LibXML::document( $$;$ ) {
	state $xmlh = XML::LibXML->new( &XML_SETTINGS( ) ) ;
	state $lwph = LWP::UserAgent->new( &UA_SETTINGS( ) ) ;
	state $url = URI::URL->new( &GIT_URL( ) ) ;

	my ( $path_url , $path_nodes ) = @_[ -2 .. -1 ] ;

	$xmlh->load_html(
		'string' => $lwph->get(
			$url->path( $path_url ) && $url
		)->content( )
	)->findnodes( $path_nodes ) ;
}

my %xpath = map {
	$_ => XML::LibXML::XPathExpression->new( scalar( <DATA> ) ) ;
} qw(profile commit) ;
my %result = map {
	$_ => {
		map {
			$_ => 0 ;
		} &HOURS( )
	} ;
} &WDAYS( ) ;

# Просмотр профиля
foreach my $project_href ( XML::LibXML::document( $login , $xpath{ 'profile' } ) ) {

	# Подсчёт количества коммитов с датой, временем и прочими
	foreach my $datetime_node ( XML::LibXML->document(
		$project_href->textContent . '/commits' , $xpath{ 'commit' } ) ) {

		no warnings ;

		my ( $wday , $hour ) = ( localtime( str2time(
			$datetime_node->textContent
		) ) )[ 6 , 2 ] ;

		$result{ + $wday + 1 }{ $hour } ++ ;
	}
}

# Собственно, вывод:
printf( '%3s' x ( 2 + &HOURS( ) ) , '' , &HOURS( ) , "\n" ) ;
printf( '%3s' x ( 2 + &HOURS( ) ) , $_ , @{ $result{ $_ } }{ &HOURS( ) } , "\n" ) foreach &WDAYS( ) ;

__DATA__
//*[@class = "pinned-repo-item-content"]//*[@class = "text-bold"]/@href
//relative-time/@datetime